cd $( dirname "$0" )

cp logback-spring.xml logback-spring-port-$1.xml

sed -i "s/\${server\.port}/$1/g" logback-spring-port-$1.xml

nohup java -jar poc-0.0.1-SNAPSHOT.jar --spring.pid.file=application.$1.pid --logging.config=logback-spring-port-$1.xml --server.port=$1 --spring.application.name=$2 >/dev/null 2>&1 &