package fr.gipmds.poc.controller;


import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import java.io.BufferedWriter;
import java.io.FileWriter;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

@RestController
public class MainController {

    Logger logger = LoggerFactory.getLogger(MainController.class);

    @GetMapping("/")
    public String getHelloWorld(){

        logger.error("Requête reçu !");
        return "Hello world";
    }


    @GetMapping("/ecriture-fichier")
    public String getEcritureFichier(){

        logger.error("Requête reçu !");

        try {
            String str = "Hello";
            BufferedWriter writer = new BufferedWriter(new FileWriter("mon-fichier.txt"));
            writer.write(str);

            writer.close();
        } catch (Exception e){
            return "Fichier non-écrit : erreur";
        }

        return "Fichier écrit";
    }
}
