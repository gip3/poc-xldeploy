package fr.gipmds.poc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.ApplicationPidFileWriter;

@SpringBootApplication
public class PocApplication {

	public static void main(String[] args) {
		SpringApplication application =
				new SpringApplication(PocApplication.class);
		application.addListeners(new ApplicationPidFileWriter());
		application.run(args);
	}

}